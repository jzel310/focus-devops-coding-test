data "digitalocean_kubernetes_versions" "versions" {}

resource "digitalocean_kubernetes_cluster" "cluster" {
    name    = "${var.project_name}-cluster"
    region  = var.region
    version = data.digitalocean_kubernetes_versions.versions.latest_version
    tags    = ["focus"]
    node_pool {
        name       = "worker-pool"
        size       = var.machine_size
        node_count = var.node_count
    }
}
