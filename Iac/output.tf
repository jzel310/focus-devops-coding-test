output "cluster-id" {
    value = digitalocean_kubernetes_cluster.cluster.id
}
output "endpoint" {
    value = digitalocean_kubernetes_cluster.cluster.endpoint
}
output "token" {
    value = digitalocean_kubernetes_cluster.cluster.kube_config[0].token
}

output "cert" {
    value = digitalocean_kubernetes_cluster.cluster.kube_config[0].cluster_ca_certificate
}

output "k8s_version" {
    value = data.digitalocean_kubernetes_versions.versions.latest_version
}
output "kube_config" {
    value = digitalocean_kubernetes_cluster.cluster.kube_config[0]
}
